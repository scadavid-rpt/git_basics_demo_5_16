# GitLab Local Development Setup Guide

This guide will walk you through setting up your local development environment to work with GitLab using Git. We'll cover creating and adding an SSH key, initializing a local repository, and managing code changes.

## 1. Create and Add Your SSH Public Key to GitLab

### Generate an RSA SSH Key:
1. Open your terminal.
2. Enter the following command to generate a new RSA SSH key:

```
ssh-keygen -t rsa -b 2048 -C "your_email@example.com"
```

Replace `your_email@example.com` with the email associated with your GitLab account. When prompted, press Enter to use the default file location, and optionally set a passphrase.

### Add Your SSH Key to the SSH-Agent:
1. Start the ssh-agent in the background:

```
eval "$(ssh-agent -s)"
```

2. Add your SSH private key to the ssh-agent:

```
ssh-add ~/.ssh/id_rsa
```

### Copy Your SSH Public Key to GitLab:
1. Display your public key and copy it:

```
cat ~/.ssh/id_rsa.pub
```

2. Log in to your GitLab account.
3. Go to your profile settings -> SSH Keys.
4. Paste your public key in the provided field, add a title to identify the key, and click "Add key."

## 2. Create a Local Directory

### Set Up Your Project Directory:
1. Create a new directory for your project and navigate into it:

```
mkdir my-project
cd my-project
```


## 3. Initialize a Git Repository

### Initialize the Repository:
1. Initialize Git in your project directory:

## 4. Add, Change, and Delete Code Locally

### Create a New File and Add Code:
1. Create a new file and open it in a text editor:

```
touch main.py # Adjust the file name based on your needs
```

2. Add some initial code to `main.py` and save the file.

### Stage and Commit Changes:
1. Add the new file to the staging area:

```
git add main.py
```

2. Commit your changes:

```
git commit -m "Initial commit with main.py"
```

### Modify Code:
1. Open `main.py`, make changes, and save.

### Stage and Commit the Updated File:
1. Add the updated file to the staging area:

```
git add main.py
```

2. Commit the changes:

```
git commit -m "Updated main.py"
```


### Delete Code:
1. Delete a file from the repository:

```
git rm main.py
```

2. Commit the deletion:

```
git commit -m "Removed main.py"
```


## 5. Push Changes to GitLab

### Link Your Local Repository to GitLab:
1. Get the SSH URL of your GitLab repository.
2. Link your local repository to the remote repository:

```
git remote add origin git@gitlab.com:username/repository.git
```

Replace `git@gitlab.com:username/repository.git` with your repository's SSH URL.

### Push Your Changes:
1. Push your changes to GitLab:

```
git push -u origin master
```


Follow these steps to set up and manage your local development environment for GitLab successfully.
