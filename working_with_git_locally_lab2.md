# Git Workflow Guide

This guide outlines how to use Git for pushing your local code changes to GitLab. We'll go through fetching and pulling from a remote repository, managing branches, merging changes, and pushing code updates.

## Step 1: Fetch and Pull from Remote

Before starting work on your local repository, ensure you have the latest changes from the remote repository:

```bash
# Fetch the latest changes from all branches
git fetch

# Switch to the branch you want to update
git checkout main

# Pull the latest changes for the branch from the remote repository
git pull origin main
```

## Step 2: Checkout Local Feature Branch

Create and switch to a new branch for your feature. This keeps your changes organized and separate from the main branch:

```bash

# Checkout a new branch named 'feature-branch'
git checkout -b feature-branch
```
## Step 3: Commit Changes

Make changes to your code locally and commit them. It’s good practice to make small, incremental commits:

```bash
# Add changes to the staging area
git add .

# Commit the changes with a descriptive message
git commit -m "Describe the changes you've made"
```

## Step 4: Merge Remote Changes on Main

Before pushing your changes, it’s important to merge any updates from the main branch into your feature branch:

``` bash
# Switch back to the main branch
git checkout main

# Pull the latest updates
git pull origin main

# Switch to your feature branch
git checkout feature-branch

# Merge changes from main into your feature branch
git merge main
```
Handle any conflicts that arise. If there are conflicts, Git will prompt you to resolve them before completing the merge.

## Step 5: Push to Remote After 3rd Edit

After making at least three edits and commits on your feature branch, push your changes to the remote repository:

```bash
# After your third commit (repeat the commit steps as necessary)
git add .
git commit -m "Final changes before push"

# Push your feature branch to GitLab
git push origin feature-branch
```

This will upload your branch and changes to GitLab. You can then create a merge request in GitLab to merge your feature branch into the main branch.



