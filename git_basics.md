# Lab 1: GitLab Basics

In this lab, you will learn how to perform basic Git operations using GitLab. This includes creating a project via the GitLab UI, creating directories, modifying code, and handling branches. We will use a simple Terraform configuration to demonstrate these operations.

## Step 1: Create a Project in GitLab

Before starting with the command line, create a new project in GitLab:

1. Log in to your GitLab account.
2. Click on the "New project" button.
3. Choose "Create blank project".
4. Fill in the project name, for example, "terraform-project".
5. Set the project visibility as required.
6. Click "Create project".

After creating your project, GitLab will provide you with a repository URL. Copy this URL for use in the next steps.

## Step 2: Clone the Repository

Once the project is created, clone it to your local machine:

```
git clone <your-gitlab-repository-url>
cd terraform-project
```


Replace `<your-gitlab-repository-url>` with the URL provided by GitLab after creating your project.

## Step 3: Create a Terraform File

Inside the project directory, create a new file named `main.tf` and open it in your favorite text editor.

## Step 4: Add Terraform Code

Add the following Terraform code to `main.tf` to define a resource for generating a random pet name:

```
provider "random" {
version = "~> 3.0"
}

resource "random_pet" "name" {
length = 3
separator = "-"
}
```


Save and close the file.

## Step 5: Add Changes to Git

Add your Terraform file to the staging area:

```
git add main.tf
```


## Step 6: Commit Changes

Commit your changes to the local repository:

```
git commit -m "Add initial Terraform configuration"
```

## Step 7: Push Changes to GitLab

Push your changes:

```
git push 
```

## Step 8: Checkout a New Branch

Create and switch to a new branch:

```
git checkout -b feature-add-more-pets
```


## Step 9: Modify Code

Modify your `main.tf` to add another `random_pet` resource or change existing parameters. For example, increase the length of the pet name:

```hcl
resource "random_pet" "name" {
length = 5
separator = "-"
}
```


## Step 10: Commit and Push the New Changes

Commit the changes in the new branch:

```
git add main.tf
git commit -am "Increase name length for random pet"
```

Push the branch to GitLab:

```
git push origin feature-add-more-pets
```


## Step 11: Merge Changes

Go to your GitLab repository in a web browser. You should see an option to create a merge request from your newly pushed branch (`feature-add-more-pets`) to `master`. Create the merge request, review the changes, and merge them if everything is correct.

Congratulations! You have completed Lab 1.

